# generator-kmcs-init [Deprecated]

> generator-kmcs-init has been deprecated.

> A [Yeoman](http://yeoman.io/) generator for installing and configuring [Karma](https://karma-runner.github.io/1.0/index.html),
[Mocha](https://mochajs.org/), [Sinon](http://sinonjs.org/) and [Chai](http://chaijs.com/).

Easy way to install, integrate and configure the popular quartet of testing tools.  
Internally it's using rollup as preprocessor. It seems that it can cause some 
problems with watching files and executing the tests when file change. [More info.](https://github.com/jlmakes/karma-rollup-preprocessor/issues/17)

## Installation and usage

Make sure you have [Node](https://nodejs.org/en/) and [npm](https://www.npmjs.com/) installed.
Then install [Yeoman](http://yeoman.io/) and generator:
```bash
npm install -g yo
npm install -g generator-kmcs-init
```

Create a new directory for your project:
```bash
mkdir my-new-project
cd my-new-project
``` 

Now you can install and configure your testing tools with:
```bash
yo kmcs-init
```

Run your tests with:
```bash
npm test
```

## Contributing
You can test kmcs-init with:
```bash
npm test
```

Additionally you can check code with ESLint:
```bash
npm run eslint
```

## License
MIT © [Paweł Halczuk](https://bitbucket.org/cctsc/)
