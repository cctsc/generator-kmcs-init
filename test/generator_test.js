var helpers = require('yeoman-test');
var assert = require('yeoman-assert');
var path = require('path');

var promptAnswers = {
  autoCapture: false,
  selectedBrowsers: [['Chrome', 'karma-chrome-launcher'],
                     ['Firefox', 'karma-firefox-launcher']],
  sourceFiles: 'src/**/*.js',
  testFiles: 'test/**/*.js',
  autoWatch: false
};

describe('kmcs-init', function() {
  before(function() {
    this.timeout(0);

    return helpers.run(path.join(__dirname, '../generators/app'))
                  .withPrompts(promptAnswers);
  });

  it('the generator can be required without throwing', function() {
    require('../generators/app/index.js');
  });

  it('generate karma.conf.js', function() {
    assert.file('karma.conf.js');
  });

  it('add scripts to package.json', function() {
    assert.jsonFileContent(
      'package.json',
      {
        scripts: {
          test: 'karma start "karma.conf.js"'
        }
      }
    );
  });
});
