var Generator = require('yeoman-generator');
var prompts = require('./templates/prompts.js');
var yosay = require('yosay');
var chalk = require('chalk');

module.exports = class extends Generator {
  initializing() {
    this.dependencies = ['karma', 'mocha', 'sinon', 'chai', 'sinon-chai',
                         'karma-sinon-chai', 'karma-mocha',
                         'karma-rollup-preprocessor', 'karma-mocha-reporter',
                         'karma-summary-reporter', 'rollup-plugin-buble'];
  }

  prompting() {
    this.log(yosay(
      `Welcome to the ${chalk.green('karma-mocha-chai-sinon-init')}!`
    ));

    return this.prompt(prompts).then(answers => {
      this.answers = answers;

      if (this.answers.autoCapture) {
        this.answers.browsersPlugins = this.answers.selectedBrowsers.map(function(elem) {
          return elem[1];
        });

        this.answers.browsersList = this.answers.selectedBrowsers.map(function(elem) {
          return elem[0];
        });
      }
    });
  }

  determineDependencies() {
    if (this.answers.autoCapture) {
      this.dependencies = this.dependencies.concat(this.answers.browsersPlugins);
    }
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('karma.conf.js'),
      this.destinationPath('karma.conf.js'),
      this.answers
    );

    this.fs.extendJSON(
      this.destinationPath('package.json'),
      {
        scripts: {
          test: 'karma start "karma.conf.js"'
        }
      }
    );

    this.log(chalk.green(`Adding useful scripts to a ${chalk.white('package.json')} file.`));
    this.log(chalk.green(`Run ${chalk.bold('karma')} to watch your files and run tests when they change.`));
  }

  install() {
    this.yarnInstall(this.dependencies, {dev: true});
    this.installDependencies({
      yarn: true,
      npm: true,
      bower: false
    });
  }

  end() {
    this.log(chalk.green('... We are done!'));
    this.log(chalk.green(`Thanks for using ${chalk.bold('kmcs-init')}. Hava a nice day!`));
  }
};
