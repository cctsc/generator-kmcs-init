var buble = require('rollup-plugin-buble');

module.exports = function(config) {
  config.set({
    autoWatch: <%= autoWatch %>,
    basePath: '',
    <% if (autoCapture) { %>
    browsers: <%- JSON.stringify(browsersList) %>,
    <% } else { %>
    browsers: [],
    <% } %>
    client: {
      chai: {
        includeStack: true
      }
    },
    colors: true,
    concurrency: Infinity,
    exclude: [],
    files: [
      { pattern: '<%= sourceFiles %>', include: false },
      '<%= testFiles %>'
    ],
    frameworks: ['mocha', 'sinon-chai'],
    logLevel: config.LOG_INFO,
    mochaReporter: { output: 'minimal' },
    port: 9876,
    preprocessors: {
      '<%= sourceFiles %>': ['rollup'],
      '<%= testFiles %>': ['rollup']
    },
    reporters: ['mocha', 'summary'],
    rollupPreprocessor: {
      format: 'iife',
      moduleName: 'test',
      plugins: [buble()],
      sourceMap: 'inline'
    },
    singleRun: false
  });
};
