'use strict';

module.exports = [
  {
    type: 'confirm',
    name: 'autoCapture',
    message: 'Do you want to capture a browser automatically?',
    default: false
  },
  {
    type: 'checkbox',
    name: 'selectedBrowsers',
    message: 'Select browsers you want to capture.',
    choices: [{name: 'Chrome', value: ['Chrome', 'karma-chrome-launcher']},
              {name: 'Firefox', value: ['Firefox', 'karma-firefox-launcher']},
              {name: 'Safari', value: ['Safari', 'karma-safari-launcher']},
              {name: 'Opera', value: ['Opera', 'karma-opera-launcher']},
              {name: 'IE', value: ['IE', 'karma-ie-launcher']},
              {name: 'Edge', value: ['Edge', 'karma-edge-launcher']},
              {name: 'PhantomJS', value: ['PhantomJS', 'karma-phantomjs-launcher']}],
    default: [],
    when: function(answers) { return answers.autoCapture; }
  },
  {
    type: 'input',
    name: 'sourceFiles',
    message: 'What is the location of your source files?',
    default: 'src/**/*.js'
  },
  {
    type: 'input',
    name: 'testFiles',
    message: 'What is the location of your test files?',
    default: 'test/**/*.js'
  },
  {
    type: 'confirm',
    name: 'autoWatch',
    message: 'Do you want Karma to watch all the files and run the tests on change?',
    default: false
  }
];
