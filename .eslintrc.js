module.exports = {
  "extends": [
    "eslint:recommended"
  ],
  "parserOptions": {
    "ecmaVersion": 8,
    "sourceType": "script",
    "ecmaFeatures": {
      "impliedStrict": true,
      "jsx": false
    }
  },
  "env": {
    "node": true,
    "es6": true,
    "commonjs": true,
    "mocha": true
  },
  "rules": {}
};